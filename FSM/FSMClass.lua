--------------------------------------------------------------------------------------
-- Finite State Machine for Lua/Cororna v 1.0

-- A prototype for all state machines in the game
-- At first create State and Transition classes then
-- intialize transition list for each state.
-- After it you can create a finite state machine

-- There are two ways to perform transition to another state:
-- 1) Change [transition].needTranstion on true ("Soft transition")
-- 2) By using FSM:transition(targetState) method ("Hard transition")
-- But prefered way is the first one

-- !!! Note that for closing it correctly you need to have transition to every state !!!
-- If you don't need to have transition to some state just create aggregate transiton
-- and add it to any state transitions list

-- Do want you want with it
-- DWYW licence ©
--------------------------------------------------------------------------------------

local FSM = {}

FSM.currentState = nil
FSM.beginState = nil

-- FSM Callbacks
FSM.onUpdate = nil
--

-- !!! DO NOT USE IT METHOD IN YOUR CLIENT CODE !!!
-- Recursive close states graph
function FSM:graphBypassClose(state)
  for i = 1, #self.states do
    if state == self.states[i] then
      return
    end
  end

  self.states[#self.states+1] = state

  for i = 1, #state.transitions do
    local nextState = state.transitions[i].targetState

    if nextState ~= nil then
      self:graphBypassClose(nextState)
    end

  end

  state:destruct()
  print(state.name.." state has been destructed")
end

function FSM:transit(nextState)
  if self.currentState ~= nil then
--    print("Exit from previous "..self.currentState.name.." state")
    self.currentState:exit()
  end

  self.currentState = nextState

  if self.currentState ~= nil then
--    print("Enter to next "..self.currentState.name.." state")
    self.currentState:enter()
  end
end

function FSM:reset()
  self:transit(self.beginState)
end

-- Finite State Machine destructor
function FSM:close()
  if self.currentState ~= nil then
    self:graphBypassClose(self.beginState)
    Runtime:removeEventListener("enterFrame", self.onUpdate)
    self.currentState = nil
    self.states = {}
    print("State machine has been closed")
  end
end

function FSM:resume()
  if self.currentState ~= nil then
    self.currentState:resume()
  end
end

function FSM:suspend()
  if self.currentState ~= nil then
    self.currentState:suspend()
  end
end

-- Finite State Machine constructor
function FSM:new(beginState)
  local stateMachine = {} -- a State Machine Object that will return
  self.__index = self
  setmetatable(stateMachine, self)

  print("Initializing state machine...")

  if beginState == nil then
    error("Begin state for Finite State Machine is nill, check FSMClass:new(beginState) arguments")
  end

  stateMachine.currentState = beginState
  stateMachine.beginState = beginState
  stateMachine.states = {}
  beginState:enter()

  stateMachine.onUpdate = function()
      if stateMachine.currentState == nil then
        return
      end

      local nextState = stateMachine.currentState:getNext()
      if nextState ~= nil then
        stateMachine:transit(nextState)
      end
  end

  -- checking for states every frame
  Runtime:addEventListener("enterFrame", stateMachine.onUpdate)

  return stateMachine
end

return FSM