local transitionQueue = {}

local QueueClass = require "src.utils.collections.queue"

transitionQueue.queue = nil
transitionQueue.isStarted = false

local function init(self, elementsArray, paramsArray, onEnd)
  local uniqueTag = os.time()

  for i = 1, #elementsArray do
    if paramsArray[i] == nil then
      error("\nTRANSITION QUEUE WARNING!\nYou didn't set parameters for the object # "..i)
    end

    paramsArray[i].tag = uniqueTag

    -- override user's onComplete
    local userOnComplete = paramsArray[i].onComplete

    -- By complete of transition dequeue the next one and start it
    paramsArray[i].onComplete = function()
      if userOnComplete then
        userOnComplete()
      end

      local element = self.queue:Dequeue()

      if element then
        transition.resume(element)
      else  -- If there is no elements use onEnd
        if onEnd then
          onEnd()
        end
        self:destruct()
      end
    end -- end of onComplete

    local trans

    if paramsArray[i].type == nil or paramsArray[i].type  == "to" then
      trans = transition.to( elementsArray[i], paramsArray[i] )
    elseif paramsArray[i].type == "from" then
      trans = transition.from( elementsArray[i], paramsArray[i] )
    elseif paramsArray[i].type == "blink" then
      trans = transition.blink( elementsArray[i], paramsArray[i] )
    else
      error("Uknown type of transition in transition queue")
    end

    transition.pause(trans) -- Pause all transitions until they don't start manually
    self.queue:Enqueue(trans)

  end -- end of "for" cycle
end

function transitionQueue:new(transitionsArray, paramsArray, onEnd)
  local newTransitionQueue = {}
  self.__index = self
  setmetatable(newTransitionQueue, self)

  newTransitionQueue.queue = QueueClass:new()
  init(newTransitionQueue, transitionsArray, paramsArray, onEnd)

  return newTransitionQueue
end

function transitionQueue:destruct()
  for i = 1, self.queue:Length() do
    local trans = self.queue:Dequeue()
    transition.cancel(trans)
    trans = nil
  end

  self.queue = nil
end

function transitionQueue:start()
  if self.isStarted == false then
    local element = self.queue:Dequeue()
    transition.resume(element)
    self.isStarted = true
  end
end

return transitionQueue