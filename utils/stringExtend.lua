----------------------------------------------
-- Extend string module
----------------------------------------------

local stringExtend = {}

-- Returns string with first charater in upper case
function string.firstToUpper(str)
    return (str:gsub("^%l", string.upper))
end

-- Resize text object respectively to border width
function string.resizeLabel(textObject, borderWidth)
  if textObject.contentWidth > borderWidth then
    local scaleModifier = borderWidth/textObject.contentWidth
    textObject:scale(scaleModifier, scaleModifier)
  end
end

return stringExtend