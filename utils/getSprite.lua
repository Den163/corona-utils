local getSprite = {}

local sheetInfo1 = require("assets.ss.ss1.ss1")
local sheetInfo2 = require("assets.ss.ss2.ss2")
local spriteSheet1 = graphics.newImageSheet( "assets/ss/ss1/ss1.png", sheetInfo1:getSheet() )
local spriteSheet2 = graphics.newImageSheet( "assets/ss/ss2/ss2.png", sheetInfo2:getSheet() )

local function SearchInSheets(spriteName)
  if sheetInfo1.frameIndex[spriteName] then
    return spriteSheet1, sheetInfo1
  end

  return spriteSheet2, sheetInfo2
end

function getSprite.getSprite(spriteName)
  local currentSheet, currentSheetInfo = SearchInSheets(spriteName)

  local sprite = display.newSprite(currentSheet, { frames={currentSheetInfo:getFrameIndex(spriteName)} })
  return sprite
end

return getSprite