local saveLoad = {}
local json = require("json")
--local openssl = require "plugin.openssl" -- UNCOMMENT WHEN RELEASE
-- local cipher = openssl.get_cipher("aes-256-cbc") -- UNCOMMENT WHEN RELEASE
local cipher = require("src.utils.encrypt") -- Test encrypter to easy testing

local randomSeed = { 20, 50, 80, 11, 12}

-- Returns the original table (second argument) if it fails to load 
function saveLoad.loadJSON(fileName, table)
  local filePath = system.pathForFile( fileName, system.DocumentsDirectory)
  
  local file = io.open(filePath, "r")
  
  if file then
    local contents = file:read( "*a" )
    io.close( file )
    local loadedTable = json.decode( contents )
    if loadedTable then
      table = loadedTable
    else 
      print("Warning!!! Failed to load table content")
    end
  else
    print("Warning!!! Failed to load file")
  end
  
  return table
end

function saveLoad.saveJSON(fileName, table)
  local filePath = system.pathForFile( fileName, system.DocumentsDirectory)
  
  local file = io.open(filePath, "w")
  local savedTablePrefs = table
  
  if file then
    file:write(json.encode( savedTablePrefs ))
    io.close(file)
  else
    print("Warning!!! Failed to save in file")
  end
end

-- Methods to work with encrypted JSON's
function saveLoad.loadDecryptedJSON(fileName, table)
  local filePath = system.pathForFile( fileName, system.DocumentsDirectory)
  
  local file = io.open(filePath, "r")
  
  if file then
    local contents = file:read( "*a" )
    io.close( file )
  -- local loadedTable = json.decode( cipher:decrypt(contents, system.getInfo("deviceID")) ) -- UNCOMMENT AND DELETE FOLLOWING LINE WHEN RELEASE
    local loadedTable = json.decode( cipher.encrypt(contents, randomSeed, true) )
    if loadedTable then
      table = loadedTable
    else
      print("IO Warning!!! Failed to load table content in "..filePath)
    end
  else
    print("IO Warning!!! Failed to load file "..filePath)
  end
  
  return table
end

function saveLoad.saveEncryptedJSON(fileName, table)
  local filePath = system.pathForFile( fileName, system.DocumentsDirectory )
  
  local file = io.open(filePath, "w")
  local savedTablePrefs = table
  
  if file then
    local originJSON = json.encode( savedTablePrefs )
    --local encryptedData = cipher:encrypt(originJSON, system.getInfo("deviceID")) -- UNCOMMENT AND DELETE FOLLOWING LINE WHEN RELEASE
    local encryptedData = cipher.encrypt(originJSON, randomSeed)
    file:write(encryptedData)
    io.close(file)
  else
    print("IO Warning!!! Failed to save file "..filePath)
  end
end

return saveLoad