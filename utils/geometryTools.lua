local geometryTools = {}

-- Params: a and b are length of cathethuses
-- Returns: hypothenuses length
function geometryTools.getHypothenuse(a, b)
-- Using Pythagoras theorem (c^2 = a^2 and b^2) to calculate hypothenuses length
  return  math.sqrt(a * a + b * b)
end

return geometryTools