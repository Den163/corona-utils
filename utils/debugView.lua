local debugView = {}

local SCREEN_TOP = 0 + display.screenOriginY
local SCREEN_BOTTOM = display.contentHeight - display.screenOriginY
local SCREEN_LEFT = 0 + display.screenOriginX
local SCREEN_RIGHT = display.contentWidth - display.screenOriginX

local fpsLabel = nil
local texturememoryLabel = nil
local ramLabel = nil  -- Total RAM

local debugGroup = nil

local fps = 0
local lastFrame = 0
local currentFrame = 0

local framesCount = 0

local function onUpdate (event)
  currentFrame = (system.getTimer() - lastFrame)
  lastFrame = system.getTimer()
  fps = 1000 / currentFrame

  debugGroup.x = -display.currentStage.x

  framesCount = framesCount + 1

  if framesCount > 120 then
    fpsLabel.text = string.format("Fps: %3.2f", fps)
    texturememoryLabel.text = string.format("Texture memory used : %d Kb", (system.getInfo("textureMemoryUsed")/1024))
    ramLabel.text = string.format("Ram used: %d Kb", collectgarbage("count"))

    framesCount = 0
  end

  debugGroup:toFront()
end

function debugView.debugOn()
  debugGroup = display.newGroup()

  fpsLabel = display.newText(
    {
      x = SCREEN_LEFT + display.screenOriginX + 100,
      y = SCREEN_BOTTOM - display.screenOriginY - 50,
      text = "",
      fontSize = 10
    }
  )

  texturememoryLabel = display.newText(
    {
      x = fpsLabel.x,
      y = fpsLabel.y + fpsLabel.contentHeight/2 + 5,
      text = "",
      fontSize = 10
    }
  )

  ramLabel = display.newText(
    {
      x = texturememoryLabel.x,
      y = texturememoryLabel.y + texturememoryLabel.contentHeight/2 + 5,
      text = "",
      fontSize = 10
    }
  )

  fpsLabel:setFillColor(1,0,0)
  texturememoryLabel:setFillColor(1,0,0)
  ramLabel:setFillColor(1,0,0)

  debugGroup:insert(fpsLabel)
  debugGroup:insert(texturememoryLabel)
  debugGroup:insert(ramLabel)

  Runtime:addEventListener("enterFrame", onUpdate)
end

return debugView