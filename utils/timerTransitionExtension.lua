------------------- SELF-CLEAN TIMERS AND TRANSITIONS -----------------------

local oldTimerPerformWithDelay = timer.performWithDelay
local oldTimerCancel = timer.cancel
local oldTransitonTo = transition.to
local oldTransitionFrom = transition.from
local oldTransitionBlink = transition.blink

timer.oldTimerPerformWithDelay = oldTimerPerformWithDelay
timer.oldTimerCancel = oldTimerCancel
transition.oldTransitonTo = oldTransitonTo
transition.oldTransitionFrom = oldTransitionFrom

timer.performWithDelay = function(delay, func)
  local timerObject = nil

  timerObject = oldTimerPerformWithDelay(delay, function()
    func()
    --print("timerObject before: ", timerObject )
    timer.cancel(timerObject)
    timerObject = nil
    --print("timerObject after: ", timerObject)
  end
  )

  return timerObject
end

-- It needs to support other API's (old timer.cancel() returns error if we try to cancel nil object)
timer.cancel = function(timerObject)
  if timerObject == nil then
   -- print("Trying to cancel a nil timer object")
    return
  end

  oldTimerCancel(timerObject)
  timerObject = nil
end

transition.to = function(object, params)
  local transitionObject = nil

  local userOnComplete = params.onComplete

  params.onComplete = function()
    if userOnComplete then
      userOnComplete()
    end

   -- print("transitionObject before: ", transitionObject)
    transition.cancel(transitionObject)
    transitionObject = nil
   -- print("transitionObject after: ", transitionObject)
  end

  transitionObject = oldTransitonTo(object, params)

  return transitionObject
end

transition.from = function(object, params)
  local transitionObject = nil

  local userOnComplete = params.onComplete

  params.onComplete = function()
    if userOnComplete then
      userOnComplete()
    end

   -- print("transitionObject before: ", transitionObject)
    transition.cancel(transitionObject)
    transitionObject = nil
   -- print("transitionObject after: ", transitionObject)
  end

  transitionObject = oldTransitionFrom(object, params)

  return transitionObject
end

transition.blink = function(object, params)
  local transitionObject = nil

  local userOnComplete = params.onComplete
  local userOnRepeat = params.onRepeat

  params.onComplete = function()
    if userOnComplete then
      userOnComplete()
    end

    print("transitionObject before: ", transitionObject)
    transition.cancel(transitionObject)
    transitionObject = nil
    print("transitionObject after: ", transitionObject)
  end

  local counter = 0

  params.repeats = params.repeats or 3

  params.onRepeat = function( target )
    if userOnRepeat then
      userOnRepeat()
    end

    transition.pause(transitionObject)

    transition.to( target, { alpha = 1, time = params.time, onComplete = function() transition.resume(transitionObject)  end })

    counter = counter + 1
    print("Count of repeats: ", counter)

    if counter == params.repeats then
      params.onComplete()
    end
  end

  transitionObject = oldTransitionBlink(object, params)

  return transitionObject
end