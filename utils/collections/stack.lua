local stack = {}

stack.elements = {}

function stack:Push( element )
--  print("The stack length is: ",  #self.elements)

  self.elements[#self.elements + 1] = element

 -- print("The stack length is: ",  #self.elements)
--  print("Element pushed: ", element)
end

function stack:Pop()
  if #self.elements == 0 then
  --  print("Stack WARNING! There are no more elements in stack")
    return nil
  end

 -- print("The stack length is: ",  #self.elements)
  local element = self.elements[#self.elements]

  self.elements[#self.elements] = nil
--  print("The stack length is: ",  #self.elements)
--  print("Element poped: ", element)

  return element
end

function stack:Length()
  return #self.elements
end

function stack:new()
  local newStack = {}

  self.__index = self
  setmetatable(newStack, self)

  newStack.elements = {}

  return newStack
end

return stack