local queue = {}

queue.elements = nil

function queue:Enqueue( element )
 -- print("The queue length is: ",  #self.elements)

  for i = #self.elements, 1, -1  do
    self.elements[i + 1] = self.elements[i]
  end

  self.elements[1] = element

 -- print("The queue length is: ",  #self.elements)
--  print("Element enqueued: ", element)
end

function queue:Dequeue()
  if #self.elements == 0 then
  --  print("QUEUE WARNING! There are no more elements in queue")
    return nil
  end

--  print("The queue length is: ",  #self.elements)
  local element = self.elements[#self.elements]

  self.elements[#self.elements] = nil
 -- print("The queue length is: ",  #self.elements)
 -- print("Element dequeued: ", element)

  return element
end

function queue:Length()
  return #self.elements
end

function queue:new()
  local newQueue = {}

  self.__index = self
  setmetatable(newQueue, self)

  newQueue.elements = {}

  return newQueue
end

return queue