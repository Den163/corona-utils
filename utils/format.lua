local format = {}

function format.formatTime(formatString, formatTime)
  local i = 0

  formatString = formatString.gsub(formatString, "%%D", string.format("%003d", (formatTime/3600/24) % 356) )
  formatString = formatString.gsub(formatString, "%%H", string.format("%02d", (formatTime/3600) % 24) )
  formatString = formatString.gsub(formatString, "%%M", string.format("%02d", (formatTime/60) % 60) )
  formatString = formatString.gsub(formatString, "%%S", string.format("%02d", formatTime % 60) )

  return formatString
end

return format