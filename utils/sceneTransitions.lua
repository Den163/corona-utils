local composer = require "composer"

composer.effectList.zoomOutIn.from.transition = easing.inBack
composer.effectList.zoomOutIn.to.transition = easing.outBack
composer.effectList.zoomOutIn.to.xStart = -display.currentStage.x
composer.effectList.zoomOutIn.from.xStart = -display.currentStage.x

composer.effectList.fromLeft.from.transition = easing.inBack
composer.effectList.fromLeft.to.transition = easing.outBack