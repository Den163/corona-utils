------------------------------------------------
-- Bezier functions for Lua/Corona v 1.0

-- Do want you want with it
-- DWYW Licence ©
------------------------------------------------

local bezier = {}

-- Interpolate point from p0 to p1 through t ( t = [0, 1] )
function bezier.LinearInterpolation(p0, p1, t)
  return (1 - t) * p0 + t * p1
end

-- Interpolate point from p0 to p2 via control point p1 and t = [0, 1]
function bezier.SquareBezier(p0, p1, p2, t)
  return (1 - t) * (1 - t) * p0 +
          2 * t * (1 - t) * p1 +
          t * t * p2
end

-- Interpolate point from p0 to p3 via control points p1, p2 and t = [0, 1]
function bezier.CubicBezier(p0, p1, p2, p3, t)
  return (1 - t) * (1 - t) * (1 - t) * p0 +
          3 * t * (1 - t) * (1 - t) * p1 +
          3 * t * t * (1 - t) * p2 +
          t * t * t * p3
end

-- !NOTE THAT FOLOWING FUNCTIONS RETURNS 2 VALUES (FOR X AND Y RESPECTIVELY)
-- startPoint, point1 is a table with x and y fields. Example:
--    pointN = {
--      x = 100,
--      y = 100
--    }

function bezier.DrawSquareBezier(startPoint, controlPoint, endPoint, t)
  return bezier.SquareBezier(startPoint.x, controlPoint.x, endPoint.x, t) ,
         bezier.SquareBezier(startPoint.y, controlPoint.y, endPoint.y , t)
end

function bezier.DrawCubicBezier(startPoint, controlPoint1, controlPoint2, endPoint, t)
  return bezier.CubicBezier(startPoint.x, controlPoint1.x, controlPoint2.x, endPoint.x, t),
         bezier.CubicBezier(startPoint.y, controlPoint1.y, controlPoint2.y, endPoint.y, t)
end

return bezier