------------------------------------
### UTILS FOR CORONA SDK ###
------------------------------------

### Include: ###
### - Finite State Machine Framework ###
### - Collections - stack and queue ###
### - Transition queue ###
### - Timer and transition extensions ###
### - String extensions ###
### - Get sprite (returns sprite from Texture Packer's spritesheet) ###
### - Bezier functions (and linear interpolation) ###
### - Debug view ###
### - Save-Load JSON ###